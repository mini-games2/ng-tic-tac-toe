import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-cell',
  templateUrl: './cell.component.html',
  styleUrls: ['./cell.component.css']
})
export class CellComponent {
  @Input() value: string = '';
  @Input() row: number = 0;
  @Input() column: number = 0;
  @Output() isChoosedEvent: EventEmitter<{row: number, column:number}> = new EventEmitter();

  alreadyChoosed: boolean = false;

  choose(){
    if(this.value === ''){
      this.isChoosedEvent.emit({row:this.row, column:this.column});
    }
  }
}
