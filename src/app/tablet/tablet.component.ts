import { Component } from '@angular/core';

@Component({
  selector: 'app-tablet',
  templateUrl: './tablet.component.html',
  styleUrls: ['./tablet.component.css']
})
export class TabletComponent {
  tablet: Array<Array<string>> = [];
  lastChoosed: string = 'O';

  ngOnInit(): void {
    this.tablet.push(['', '', '']);
    this.tablet.push(['', '', '']);
    this.tablet.push(['', '', '']);
  }

  private reset(): void{
    this.tablet.forEach((rows, i) => {
      rows.forEach((item, j) => {
        this.tablet[i][j] = '';
      })
    });
    this.lastChoosed = 'O';
  }

  chooseCell(event: {row:number, column:number}): void{
    const next = this.nextPlayer();
    this.tablet[event.row][event.column] = next;
    this.lastChoosed = next;
    if(this.isPartyFinished(event.row, event.column, next)){
      const finalMessage = `Party is finished the winner is ${next}`;
      alert(finalMessage);
      this.reset();
    }
  }

  private isPartyFinished(row: number, column: number, value: string): boolean{
    //check the line
    for(let i = 0; i < this.tablet[row].length ; i++){
      if(this.tablet[row][i] !== value || this.tablet[row][i] === ''){
        break;
      } 
      if(i === this.tablet[row].length - 1) return true;
    }
    //check the column
    for(let i = 0; i < this.tablet.length ; i++){
      if(this.tablet[i][column] !== value || this.tablet[row][column] === ''){
        break;
      } 
      if(i === this.tablet.length - 1) return true;
    }
    //check the diagonal
    if ((this.tablet[0][0] == value &&
      this.tablet[1][1] == value &&
      this.tablet[2][2] == value) ||
  (this.tablet[0][2] == value &&
    this.tablet[1][1] == value &&
    this.tablet[2][0] == value)){
      return true;
    }

    return false;
  }

  private nextPlayer(): string{
    return this.lastChoosed === 'O' ? 'X' : 'O';
  }
}
